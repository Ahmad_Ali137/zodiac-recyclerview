package com.riis.horoscope

import java.util.*

//CREATING THE MODEL LAYER FOR THE APP

//Zodiac class creates objects which each hold information about a horoscope
data class Zodiac(val id: UUID = UUID.randomUUID(), //random unique id
                     var title: String = "", //descriptive title
                     var imageId: String = "") //image id from resources

