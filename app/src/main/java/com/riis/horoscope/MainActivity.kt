package com.riis.horoscope

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //asking the FragmentManager for the current fragment being used in the fragment_container
        val currentFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_container)

        //if there is no current fragment, create a fragment from CrimeFragment.kt or CrimeListFragment.kt
        if (currentFragment == null) {
            val fragment = ZodiacListFragment.newInstance()

            //Create a new fragment transaction, include one add operation in it, and then commit it
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, fragment)//adds the fragment to the container
                .commit()
        }
    }
}