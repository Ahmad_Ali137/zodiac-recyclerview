package com.riis.horoscope

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

private const val TAG = "ZodiacListFragment"

//ZodiacListFragment is a fragment which controls the recycler view designed by fragment_zodiac_list.xml when hosted by MainActivity.kt
class ZodiacListFragment: Fragment(){

    //DEFINING CLASS VARIABLES
    private lateinit var zodiacRecyclerView: RecyclerView
    private var adapter: ZodiacAdapter? = null

    private val zodiacListViewModel: ZodiacListViewModel by lazy { //associating the fragment with ZodiacListViewModel.kt
        ViewModelProviders.of(this).get(ZodiacListViewModel::class.java)
    }

    //WHAT HAPPENS WHEN THE FRAGMENT ZodiacListFragment IS CREATED
    override fun onCreate(savedInstanceState: Bundle?) { //passes information saved in the bundle or null if empty
        super.onCreate(savedInstanceState) //creates the fragment using the info passed to savedInstanceState
        Log.d(TAG, "Total Zodiac signs: ${zodiacListViewModel.zodiacSigns.size}") //logs the size of the zodiacSigns list in the ViewModel
    }

    //WHAT HAPPENS WHEN THE FRAGMENT VIEW designed using fragment_crime_list.xml IS CREATED
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_zodiac_list, container, false)

        //referencing the RecyclerView from the layout fragment_zodiac_list.xml using its View ID
        zodiacRecyclerView = view.findViewById(R.id.zodiac_recycler_view) as RecyclerView

        //giving the RecyclerView a LayoutManager which is required to make it work
        //LinearLayoutManager positions the items in its list vertically on the screen
        zodiacRecyclerView.layoutManager = LinearLayoutManager(context)

        updateUI()

        return view
    }

    private fun updateUI() {
        val zodiaclist = zodiacListViewModel.zodiacSigns
        adapter = ZodiacAdapter(zodiaclist)
        zodiacRecyclerView.adapter = adapter
    }

    //CREATING AND IMPLEMENTING A ViewHolder TO WRAP THE ITEM VIEWS FOR A RecyclerView
    private inner class ZodiacHolder(view: View)
        : RecyclerView.ViewHolder(view),View.OnClickListener { //ViewHolder class will hold on to the provided view in a property named itemView

        private lateinit var zodiacSign: Zodiac

        //referencing child views in the item view defined by list_item_zodiac.xml using their view IDs
        private val titleTextView: TextView = itemView.findViewById(R.id.zodiac_title)
        private val IconImageView: ImageView = itemView.findViewById(R.id.zodiac_icon)


        init {
            itemView.setOnClickListener(this)
        }

        //function used to tell the ViewHolder to bind to a zodiac sign
        fun bind(sign: Zodiac) {
            this.zodiacSign = sign

            //setting the text for the child text views of the zodiac sign itemView in the ViewHolder
            titleTextView.text = this.zodiacSign.title
            when (zodiacSign.title){
                "Aquarius" -> IconImageView.setImageResource(R.drawable.aquarius)
                "Aries" -> IconImageView.setImageResource(R.drawable.aries)
                "Cancer" -> IconImageView.setImageResource(R.drawable.cancer)
                "Capricorn" -> IconImageView.setImageResource(R.drawable.capricorn)
                "Gemini" -> IconImageView.setImageResource(R.drawable.gemini)
                "Leo" -> IconImageView.setImageResource(R.drawable.leo)
                "Libra" -> IconImageView.setImageResource(R.drawable.libra)
                "Pisces" -> IconImageView.setImageResource(R.drawable.pisces)
                "Sagittarius" -> IconImageView.setImageResource(R.drawable.sagittarius)
                "Scorpio" -> IconImageView.setImageResource(R.drawable.scorpio)
                "Taurus" -> IconImageView.setImageResource(R.drawable.taurus)
                "Virgo" -> IconImageView.setImageResource(R.drawable.virgo)
            }

        }

        override fun onClick(v: View) {
            Toast.makeText(context, "${zodiacSign.title} pressed!", Toast.LENGTH_SHORT)
                .show()
        }

    }

    //CREATING AND IMPLEMENTING AN Adapter TO POPULATE THE RecyclerView
    private inner class ZodiacAdapter(var signs: List<Zodiac>)//accepts and stores a list of Zodiac objects as input
        : RecyclerView.Adapter<ZodiacHolder>() {

        //creates a view to display, wraps the view in a view holder, and returns the result.
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
                : ZodiacHolder {
            //inflates list_item_view.xml and passes the resulting view to a new instance of ZodiacHolder
            val view = layoutInflater.inflate(R.layout.list_item_zodiac, parent, false)
            return ZodiacHolder(view)
        }

        //returns the number of items in the list of zodiac signs
        override fun getItemCount() = signs.size

        //populates a given holder with the zodiac sign from a given position in the zodiac list
        override fun onBindViewHolder(holder: ZodiacHolder, position: Int) {
            val crime = signs[position] //selecting the crime
            holder.bind(crime)//binding the holder to the crime
        }
    }

    //FUNCTION THAT ACTIVITIES CAN CALL TO GET AN INSTANCE OF THE FRAGMENT
    companion object {
        fun newInstance(): ZodiacListFragment {
            return ZodiacListFragment()
        }
    }

}
