package com.riis.horoscope

import androidx.lifecycle.ViewModel

//ZodiacListViewModel is a ViewModel used to store a list of Zodiac objects for ZodiacListFragment.kt
//this ViewModel is destroyed when the fragment from ZodiacListFragment.kt is destroyed
class ZodiacListViewModel : ViewModel() {

    val zodiacSigns = mutableListOf<Zodiac>()
    val zodiacNames = ArrayList<String>(listOf("Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn", "Aquarius", "Pisces"))

    init{
        for (name in zodiacNames){
            val zodiac = Zodiac()
            zodiac.title = name
            zodiacSigns += zodiac
        }
    }

}